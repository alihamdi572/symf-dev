<?php

namespace App\Controller;

use App\Entity\Location;
use App\Form\LocationType;
use App\Repository\LocationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/location')]
class LocationController extends AbstractController
{
    #[Route('/', name: 'app_location_index', methods: ['GET'])]
    public function index(LocationRepository $locationRepository): Response
    {
        return $this->render('location/index.html.twig', [
            'locations' => $locationRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_location_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $location = new Location();
        $form = $this->createForm(LocationType::class, $location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $prix = 0;
            //Nbre de jours
            $nbrj = $location->getDateA()->diff($location->getDateD())->days;

            //prixJour Voiture
            $prixJour = $location->getVoiture()->getPrixJour();

            //Calcul prix
            $prix = $prixJour * $nbrj;

            $location->setPrix($prix);
            $entityManager->persist($location);
            $entityManager->flush();

            return $this->redirectToRoute('app_location_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('location/new.html.twig', [
            'location' => $location,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_location_show', methods: ['GET'])]
    public function show(Location $location): Response
    {
        return $this->render('location/show.html.twig', [
            'location' => $location,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_location_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Location $location, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(LocationType::class, $location);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_location_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('location/edit.html.twig', [
            'location' => $location,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_location_delete', methods: ['POST'])]
    public function delete(Request $request, Location $location, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$location->getId(), $request->request->get('_token'))) {
            $entityManager->remove($location);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_location_index', [], Response::HTTP_SEE_OTHER);
    }
    public function testShouldDisplayLocationIndex()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location Index');
    }

    public function testShouldDisplayCreateNewLocation()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create New Location');
    }

    public function testShouldAddNewLocation()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();

        $form = $buttonCrawlerNode->form([
            // Fill in the form fields with valid data
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Location details');
    }

    public function testShouldDisplayLocationDetails()
    {
        // Assuming you have a location with ID 1 in your database
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location/1');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location Details');
    }

    public function testShouldEditLocation()
    {
        // Assuming you have a location with ID 1 in your database
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/location/1/edit');

        $buttonCrawlerNode = $crawler->selectButton('Save Changes');

        $form = $buttonCrawlerNode->form();

        $form = $buttonCrawlerNode->form([
            // Fill in the form fields with updated data
        ]);

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Updated Location details');
    }

    public function testShouldDeleteLocation()
    {
        // Assuming you have a location with ID 1 in your database
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('POST', '/location/1');

        // Include CSRF token handling if required

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Location Index');
    }
    
}
