<?php

namespace App\Tests;

use Symfony\Component\Panther\PantherTestCase;

class FunctionalTest extends PantherTestCase
{
    public function testShouldDisplayClients()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Client index');
    }

    public function testShouldDisplayCreateNewClient()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client/new');
 
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Client');
    }

    public function testShouldAddNewClient()
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/client/new');
    
        $buttonCrawlerNode = $crawler->selectButton('Save');
    
        $form = $buttonCrawlerNode->form();
    
        $uuid = uniqid();
    
        $form = $buttonCrawlerNode->form([
            'client[nom]'    => 'New Client' . $uuid,
            'client[prenom]' => 'Test',
            'client[adresse]' => 'Test Address',
            'client[cin]'     => 'TestCIN',
        ]);
        
        $client->submit($form);
        
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'New Client' . $uuid);
    }
    public function testShouldAddNewLocation()
{
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/location/new');

    // Fill in the form fields with valid data
    $form = $crawler->selectButton('Save')->form([
        'location[dateD]' => '2024-01-01',
        'location[dateA]' => '2024-01-10',
        // Add other form fields as needed
    ]);

    $client->submit($form);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Location details');
}

public function testShouldEditLocation()
{
    // Assuming you have a location with ID 1 in your database
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/location/1/edit');

    // Fill in the form fields with updated data
    $form = $crawler->selectButton('Save Changes')->form([
        'location[dateD]' => '2024-02-01',
        'location[dateA]' => '2024-02-10',
        // Update other form fields as needed
    ]);

    $client->submit($form);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Updated Location details');
}

public function testShouldDeleteLocation()
{
    // Assuming you have a location with ID 1 in your database
    $client = static::createClient();
    $client->followRedirects();
    
    // Fetch the CSRF token
    $csrfToken = $client->getContainer()->get('security.csrf.token_manager')->getToken('delete1')->getValue();
    
    $client->request('POST', '/location/1', ['_token' => $csrfToken]);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Location Index');
}
public function testShouldDisplayVoitureList()
{
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/voiture');

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Liste des Voitures');
}

public function testShouldAddNewVoiture()
{
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/addVoiture');

    // Assuming you have a valid form submission here
    $form = $crawler->selectButton('Save')->form();
    // Fill in the form fields with valid data
    $form['voiture_form[serie]'] = 'ABC123';
    // ... Fill in other form fields as needed

    $client->submit($form);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Voiture details');
}
public function testShouldDeleteVoiture()
{
    // Assuming you have a voiture with ID 1 in your database
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/voiture/1');

    // Include CSRF token handling if required

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Liste des Voitures');
}

public function testShouldUpdateVoiture()
{
    // Assuming you have a voiture with ID 1 in your database
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('GET', '/updateVoiture/1');

    // Assuming you have a valid form submission here
    $form = $crawler->selectButton('Save Changes')->form();
    // Fill in the form fields with updated data
    $form['voiture_form[serie]'] = 'UpdatedABC123';
    // ... Fill in other form fields as needed

    $client->submit($form);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Voiture details');
}

public function testShouldSearchVoiture()
{
    $client = static::createClient();
    $client->followRedirects();
    $crawler = $client->request('POST', '/searchVoiture', ['input_serie' => 'ABC123']);

    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('body', 'Searched Voiture details');
}
}
