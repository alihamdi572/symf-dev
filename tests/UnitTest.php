<?php

namespace App\Tests;
use App\Entity\Client;
use App\Entity\Location;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use App\Entity\Voiture;


class UnitTest extends TestCase
{
    public function testClient(): void
    {
        // Create a Client instance
        $client = new Client();

        // Set client properties
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdresse('123 Ezzahra');
        $client->setCin('ABC123');

        // Create a Location instance
        $location = new Location();
        $location->setDateD(new \DateTime('2024-01-15'));
        $location->setDateA(new \DateTime('2024-01-20'));
        $location->setPrix(100.00);

        // Add the Location to the Client's locations collection
        $client->addLocation($location);

        // Test the getters
        $this->assertEquals('John', $client->getNom());
        $this->assertEquals('Doe', $client->getPrenom());
        $this->assertEquals('123 Ezzahra', $client->getAdresse());
        $this->assertEquals('ABC123', $client->getCin());

        // Test the one-to-many relationship
        $locations = $client->getLocations();
        $this->assertInstanceOf(ArrayCollection::class, $locations);
        $this->assertCount(1, $locations);
        $this->assertSame($location, $locations->first());
        $this->assertSame($client, $location->getClient());

        // Remove the Location from the Client's locations collection
        $client->removeLocation($location);

        // Test the removal
        $this->assertCount(0, $client->getLocations());
        $this->assertNull($location->getClient());
    }



    public function testLocation(): void
    {
        // Create a Location instance
        $location = new Location();
        
        // Set location properties
        $location->setDateD(new \DateTime('2024-01-15'));
        $location->setDateA(new \DateTime('2024-01-20'));
        $location->setPrix(100.00);

        // Create a Client instance
        $client = new Client();
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdresse('123 Ezzahra');
        $client->setCin('ABC123');

        // Add the Location to the Client's locations collection
        $client->addLocation($location);

        // Test the getters
        $this->assertEquals(new \DateTime('2024-01-15'), $location->getDateD());
        $this->assertEquals(new \DateTime('2024-01-20'), $location->getDateA());
        $this->assertEquals(100.00, $location->getPrix());

        // Test the many-to-one relationship
        $this->assertInstanceOf(Client::class, $location->getClient());
        $this->assertSame($client, $location->getClient());

        // Test the removal from Client's locations collection
        $client->removeLocation($location);
        $this->assertNull($location->getClient());
    }



    public function testVoiture(): void
    {
        // Create a Voiture instance
        $voiture = new Voiture();

        // Set voiture properties
        $voiture->setSerie('ABC123');
        $voiture->setDateMM(new \DateTime('2024-01-15'));
        $voiture->setPrixJour(50.00);

        // Create a Location instance
        $location = new Location();
        $location->setDateD(new \DateTime('2024-01-15'));
        $location->setDateA(new \DateTime('2024-01-20'));
        $location->setPrix(100.00);

        // Add the Location to the Voiture's locations collection
        $voiture->addLocation($location);

        // Test the getters
        $this->assertEquals('ABC123', $voiture->getSerie());
        $this->assertEquals(new \DateTime('2024-01-15'), $voiture->getDateMM());
        $this->assertEquals(50.00, $voiture->getPrixJour());

        // Test the one-to-many relationship with Location
        $locations = $voiture->getLocations();
        $this->assertInstanceOf(ArrayCollection::class, $locations);
        $this->assertCount(1, $locations);
        $this->assertSame($location, $locations->first());
        $this->assertSame($voiture, $location->getVoiture());

        // Remove the Location from the Voiture's locations collection
        $voiture->removeLocation($location);

        // Test the removal
        $this->assertCount(0, $voiture->getLocations());
        $this->assertNull($location->getVoiture());
    }
   
}
